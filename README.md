# MapsComboTest
---
This is a demo project, created to test the possibility of combining [Google Maps SDK for Android](https://cloud.google.com/maps-platform/maps/) for showing the map and [HERE Location Services](https://www.here.com/products/location-based-services) for place search and routing. It is possible because everything we need is coordinates (latitude and longitude).

**Caution!** The project is just for testing, so it doesn't handle all edge cases and may contain bugs.
### How to run
To run this project, you need to put to your `local.properties` GoogleMaps **API KEY**, HereMaps **APP ID** and **APP CODE**.
```
...
googleMapsApiKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
hereMapsAppId=YYYYYYYYYYYYYYYYYYYY
hereMapsAppCode=ZZZZZZZZZZZZZZZZZZZZZZ
```
Also, you need to download **HERE-sdk.aar** and put it to `/app/libs` folder.

### Documentation for API used
Google Maps SDK for Android

- [showing markers](https://developers.google.com/maps/documentation/android-sdk/map-with-marker)
- [showing polylines](https://developers.google.com/maps/documentation/android-sdk/polygon-tutorial)

HereMaps Android SDK Starter:

- [search for places](https://developer.here.com/documentation/android-starter/dev_guide/topics/places.html)
- [routing](https://developer.here.com/documentation/android-starter/dev_guide/topics/routing-starter.html)