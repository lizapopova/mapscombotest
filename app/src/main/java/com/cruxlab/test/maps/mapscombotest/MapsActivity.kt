package com.cruxlab.test.maps.mapscombotest

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.MapEngine
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.routing.RouteManager
import com.here.android.mpa.routing.RouteOptions
import com.here.android.mpa.routing.RoutePlan
import com.here.android.mpa.routing.RouteResult
import com.here.android.mpa.search.DiscoveryResultPage
import com.here.android.mpa.search.ErrorCode
import com.here.android.mpa.search.PlaceLink
import com.here.android.mpa.search.SearchRequest
import kotlinx.android.synthetic.main.activity_maps.*
import java.util.*

fun LatLng.toGeoCoordinate() = GeoCoordinate(latitude, longitude)

fun GeoCoordinate.toLatLng() = LatLng(latitude, longitude)

fun Int.parseRGB() = Triple(Color.red(this), Color.green(this), Color.blue(this))

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap

    private var polylines = LinkedList<Polyline>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.moveCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.fromLatLngZoom(INITIAL_POSITION, INITIAL_ZOOM)
            )
        )
        map.setOnMarkerClickListener { marker ->
            requestRoute(marker.position.toGeoCoordinate())
            marker.showInfoWindow()
            true
        }
        initHereApi {
            initSearchField()
            showInitialMarker()
        }
    }

    private fun initHereApi(callback: () -> Unit) {
        val mapEngine = MapEngine.getInstance()
        mapEngine.init(applicationContext) { error ->
            if (error == OnEngineInitListener.Error.NONE) {
                callback()
            } else {
                Log.d(TAG, error.name)
                showError(error.name)
            }
        }
    }

    private fun initSearchField() {
        searchInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                requestPlaces(s.toString())
            }
        })
        searchInput.setText(INITIAL_SEARCH_QUERY)
    }

    private fun showMarkers(page: DiscoveryResultPage) {
        map.clear()
        for (item in page.items) {
            if (item !is PlaceLink) {
                continue
            }
            val pos = item.position.toLatLng()
            Log.d(TAG, "${item.title} $pos ${item.vicinity}")
            map.addMarker(
                MarkerOptions()
                    .position(pos)
                    .title(item.title)
            )
        }
        showInitialMarker()
    }

    private fun showInitialMarker() {
        map.addMarker(
            MarkerOptions().position(INITIAL_POSITION).icon(
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)
            )
        )
    }

    private fun requestPlaces(query: String) {
        val request = SearchRequest(query).setSearchCenter(INITIAL_POSITION.toGeoCoordinate())
        request.collectionSize = SEARCH_CNT
        request.execute { page, errorCode ->
            if (errorCode != ErrorCode.NONE) {
                Log.d(TAG, "errorCode=$errorCode")
                showError(errorCode.name)
            } else {
                showMarkers(page)
            }
        }
    }

    private fun requestRoute(dest: GeoCoordinate) {
        val routeManager = RouteManager()
        val routePlan = RoutePlan()
        routePlan.addWaypoint(INITIAL_POSITION.toGeoCoordinate())
        routePlan.addWaypoint(dest)
        val routeOptions = RouteOptions()
        routeOptions.transportMode = RouteOptions.TransportMode.CAR
        routeOptions.routeType = RouteOptions.Type.FASTEST
        routeOptions.routeCount = 1
        routePlan.routeOptions = routeOptions
        routeManager.calculateRoute(routePlan, object : RouteManager.Listener {
            override fun onCalculateRouteFinished(
                error: RouteManager.Error,
                routeResult: MutableList<RouteResult>
            ) {
                if (error == RouteManager.Error.NONE) {
                    if (routeResult.size > 0) {
                        val points = routeResult[0].route.routeGeometry
                        showGradientRoute(points, START_COLOR, END_COLOR)
                        Log.d(TAG, "Route found")
                    }
                } else {
                    Log.d(TAG, error.name)
                    showError(error.name)
                }
            }

            override fun onProgress(p0: Int) {
            }
        })
    }

    private fun showError(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    private fun showGradientRoute(route: List<GeoCoordinate>, startColor: Int, endColor: Int) {
        clearPolylines()
        val routePoints = route.map { it.toLatLng() }
        if (routePoints.size < 2) {
            return
        }
        val (startR, startG, startB) = startColor.parseRGB()
        val (endR, endG, endB) = endColor.parseRGB()
        val diffR = endR - startR
        val diffG = endG - startG
        val diffB = endB - startB
        var startIndex = 0
        for (percent in 1..100) {
            val fraction = percent / 100f
            val points = (routePoints.size * fraction).toInt()
            if (startIndex == points) {
                continue
            }
            val color = createColor(
                (startR + diffR * fraction) / 255f,
                (startG + diffG * fraction) / 255f,
                (startB + diffB * fraction) / 255f
            )
            val options = PolylineOptions().color(color)
            for (index in Math.max(0, startIndex - 1) until points) {
                options.add(routePoints[index])
            }
            startIndex = points
            val polyline = map.addPolyline(options)
            polyline.endCap = RoundCap()
            polylines.add(polyline)
        }
    }

    private fun clearPolylines() {
        for (polyline in polylines) {
            polyline.remove()
        }
        polylines.clear()
    }

    private fun createColor(red: Float, green: Float, blue: Float): Int {
        return -0x1000000 or
            ((red * 255.0f + 0.5f).toInt() shl 16) or
            ((green * 255.0f + 0.5f).toInt() shl 8) or
            (blue * 255.0f + 0.5f).toInt()
    }

    companion object {
        const val TAG = "MapsComboTest"
        const val INITIAL_SEARCH_QUERY = "Airport"
        const val INITIAL_ZOOM = 10f
        val INITIAL_POSITION = LatLng(34.05349, -118.24532)
        const val SEARCH_CNT = 10
        val START_COLOR = Color.parseColor("#E21952")
        val END_COLOR = Color.parseColor("#FCD557")
    }
}
